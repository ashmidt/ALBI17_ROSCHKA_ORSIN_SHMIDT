import time
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord

def readBreaking(pathToFile, k_size):
    file_open = open(pathToFile).readlines()
    if (k_size <= 0):
        print("K_mer must be positive")
        return None
    matrix =[]                                   #Dynamic Matrix initialisation 2x2
    matrix_line=[]                               #Dynamic Matrix initialisation 2x2
    header=True                                  #Header line in Fasta
    for line in file_open:
        if header:
            header=False
        else:
            line=line.rstrip('\n')               #delete new line
            i = 0
            while True:
                matrix_line.append(line[i:i+k_size]) #save results
                i = i + 1
                if(i+k_size > len(line)):         #Stop Condition
                    break
            matrix.append(matrix_line)            #add new Element in Matrix
            matrix_line=[]
    return matrix

def createdictfromatrix(Matrix):
    m=len(Matrix)
    n=len(Matrix[0])
    SPdict={}

    for i in range(m):
        for j in range(n):
            kmer= Matrix[i][j]
            if kmer[:-1] in SPdict:                        #check if suffix_prefix  is in dictionary
                SPdict[kmer[:-1]][0].append(kmer)          #add suffix_prefix and path
            else:
                SPdict.update({kmer[:-1]:[[kmer],[]]})     #add path
            if kmer[1:] in SPdict:
                SPdict[kmer[1:]][1].append(kmer)           #add suffix_prefix and path
            else:
                SPdict.update({kmer[1:]:[[],[kmer]]})      #add path
    return(SPdict)




def maximalnonbranchingpaths(path_dictionary):
    len_suffix_prefix = len(next(iter(path_dictionary)))  #calculate the lenght of suffix_prefix
    paths = []
    for suffix_prefix in path_dictionary:
        if (len(path_dictionary[suffix_prefix][0])!=1 or len(path_dictionary[suffix_prefix][1])!=1): #check if path exists
            if len(path_dictionary[suffix_prefix][0])>0:
                for outgoing_edge in path_dictionary[suffix_prefix][0]:
                    nonbranchingpath = suffix_prefix                                                   #check paths for defined suffix_prefix
                    try:
                        while len(path_dictionary[outgoing_edge[-len_suffix_prefix:]][0])==1 and len(path_dictionary[outgoing_edge[-len_suffix_prefix:]][1])==1:
                            nonbranchingpath=nonbranchingpath+outgoing_edge[-len_suffix_prefix+1:]           #increment path
                            outgoing_edge=path_dictionary[outgoing_edge[-len_suffix_prefix:]][0][0]
                        nonbranchingpath = nonbranchingpath + outgoing_edge[-len_suffix_prefix+1:]           #save the edge
                    except:
                        pass
                    paths.append(SeqRecord(Seq(nonbranchingpath), id='contig', description="contig_n_deltocephalinicola__reads__100")) #convert results
    SeqIO.write(paths, "result.fasta", "fasta") # save results in fasta

k_size=100
pathToFile ="C:/Users/alex/Dropbox/Workspaces/py_ws/FU/ALBI17_ROSCHKA_ORSIN_SHMIDT/praktikum2/n_deltocephalinicola__reads__100.txt"

start_time = time.time()
readbreak = readBreaking(pathToFile, k_size)
suffixpreffixlib =createdictfromatrix(readbreak)
maximalnonbranchingpaths(suffixpreffixlib)
print(time.time()-start_time)
