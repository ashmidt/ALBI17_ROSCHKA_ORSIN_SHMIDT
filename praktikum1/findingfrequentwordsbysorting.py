from algorithms.sorting import quick_sort

# fastFrequentWords

import math

def findingfrequentwordsbysorting(Text,k):
    FrequentPatterns=[] # generiere FrequentPatterns array
    index =[] # generiere Frequency index array
    count=[] # generiere Frequency count array
    for i in range(0, len(Text)-k):
        Pattern = Text[i:i+k]
        index.append(patternToNumber(Pattern)) # speichere Patternzahl als index
        count.append(1)  #erstell Platzhalter mit Wert 1
    sortedindex = sort(index)
    for i in range(len(Text)-k):
        if sortedindex[i]==sortedindex[i-1]:
            count[i]=count[i-1]+1 #zaelen alle gleiche Indexe
    maxCount=max(count)
    for i in range(len(Text)-k):
        if count[i]==maxCount:
            Pattern = numberToPattern(sortedindex[i],k) #etschlusse Index
            FrequentPatterns.append(Pattern) #fugt passende Elemnte rein
    print("findingfrequentwordsbysorting: ", list(set(FrequentPatterns)))


def sort(index):
    return quick_sort.sort(index)

def computingFrequencies(text,k):   # generiere Frequency Array
    frequencyArray = [] # leere Liste fuer k-mere im Text
    for i in range(0, (int(math.pow(4, k)))):
        frequencyArray.append(0)    # fuelle leeres Frequency Array mit Nullen
    for i in range(0, (len(text)-k+1)): # iteriere ueber Laenge des Textes - Fenstergroesse, + 1 weil Index bei 0 startet
        pattern = text[i:i+k]   # betrachte Fenster ueber Text mit Laenge k
        number = patternToNumber(pattern)    # wandle Pattern in Zahl um
        if number == "XXX":     # invalid Symbol
            error = ["X"]
            return error
        frequencyArray[number] = frequencyArray[number]+1   # gehe zu naechstem Index
    return frequencyArray

def patternToNumber(pattern):   # wandle Pattern in Zahl um, fuer computingFrequencies
    if not pattern: # falls Pattern leer
        return 0
    symbol = pattern[-1]    # letztes Symbol des Patterns
    prefix = pattern[:-1]   # Prefix des Patterns
    number = symbolToNumber(symbol)     # wandle letztes Symbol des Patterns in zugehoerige Zahl um
    if number == 4:     # invalid Symbol
        error = "XXX"
        return error
    return 4* patternToNumber(prefix) + number  # generiere Zahl fuer Pattern, Basis 4 wegen 4 Nukleotiden

def symbolToNumber(symbol): # wandle Nukleotid in entsprechenden Repraesentanten um, fuer patternToNumber
    if symbol == "a":
        return 0
    elif symbol == "c":
        return 1
    elif symbol == "g":
        return 2
    elif symbol == "t":
        return 3
    else:
        return 4 #invalid symbol

def numberToPattern(index,k):   # wandle Zahl in Pattern um, fuer fastFrequentWords
    if k == 1:  # falls Fenstergroesse k=1, sofort Zahlen zu Basen codieren
        return numberToSymbol(index)
    prefixIndex = math.floor(index / 4)     # Quotient aus Index des Frequency Arrays und Basis 4, Prefix des Patterns
    remainder = index % 4   # Rest aus Index modulo 4(Basis), aequivalent zu patternToNumber(Prefix(Pattern))
    symbol = numberToSymbol(remainder)  # wandle Zahl in Repraesentant des Nukleotids um
    prefixPattern = numberToPattern(prefixIndex, k-1)   # rekursives Vorgehen fuer Prefix des Patterns mit verringerter Fenstergroesse
    return prefixPattern + symbol   # uebergebe konkatenierte Nukleotide = Pattern

def numberToSymbol(number): # wandle Repraesentanten in zugehoeriges Nukleotid um, fuer numberToPattern
    symb = ""
    if   number == 0:
        symb = "a"
    elif number == 1:
        symb = "c"
    elif number == 2:
        symb = "g"
    else:   # number > 3 werden schon in symbolToNumber abgefangen, siehe invalid Symbol
        symb = "t"
    return symb