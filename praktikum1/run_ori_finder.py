from Bio import SeqIO
import time
from frequentWords import frequentWords
from fastFrequentWords import fastFrequentWords
from findingfrequentwordsbysorting import findingfrequentwordsbysorting

def main():
    print("Thermotoga_petrophila")
    Thermotoga_petrophila = SeqIO.read("Thermotoga_petrophila.fna", "fasta")
    start_time=time.clock()
    frequentWords(str(Thermotoga_petrophila.seq), 9)
    print("Zeit gebraucht: ", time.clock()-start_time)

    start_time = time.clock()
    fastFrequentWords(Thermotoga_petrophila.seq, 9)
    print("Zeit gebraucht: ", time.clock() - start_time)

    start_time = time.clock()
    findingfrequentwordsbysorting(Thermotoga_petrophila.seq, 9)
    print("Zeit gebraucht: ", time.clock() - start_time)

    print("Vibrio_cholerae")
    Vibrio_cholerae = SeqIO.read("Vibrio_cholerae.fna", "fasta")

    start_time=time.clock()
    frequentWords(str(Vibrio_cholerae.seq), 9)
    print("Zeit gebraucht: ", time.clock()-start_time)

    start_time = time.clock()
    fastFrequentWords(Vibrio_cholerae.seq, 9)
    print("Zeit gebraucht: ", time.clock() - start_time)

    start_time = time.clock()
    findingfrequentwordsbysorting(Vibrio_cholerae.seq, 9)
    print("Zeit gebraucht: ", time.clock() - start_time)

if __name__ == '__main__':
    main()