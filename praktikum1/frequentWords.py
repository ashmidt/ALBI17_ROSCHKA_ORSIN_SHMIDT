def frequentWords(Text, k):                         #erstelle Funktion mit Namen frequencyWords und lege übergebene Parameter Text und k-mer-Länge k fest
    freqP=[]                                        #erstelle leeres Array freqP, in das später häufigste k-mere aufgenommen werden sollen
    Count=[]                                        #erstelle leeres Array Count zum Zwischenspeichern des Counters für die Häufigkeiten jedes k-mers
    n=len(Text)                                     #speichere unter der Variable n die Länge des übergebenen Texts

    for i in range(n-k+1):                          #erstelle for-Schleife, um durch Text zu gehen und jedes k-mer im Text einzeln nacheinander anzuschauen
        Pattern = Text[i:i+k]                       #speichere unter der Variable Pattern das aktuelle k-mer ab
        Count.append(patternCount(Text, Pattern))   #füge die Häufigkeit (durch PatternCount gezählt) jedes k-mers in das Array Count ein
    maxCount = max(Count)                           #speichere unter der Variable maxCount den höchsten Counter ab, um dann gleich ein paar Zeilen weiter die häufigsten k-mere zu finden

    for j in range(n-k+1):                          #erstelle for-Schleife mit gleichen Parametern wie for-Schleife für Textdurchgang, um durch Count-Array gehen zu können und für jede Stelle im Text passendnen Counter zu finden
        if Count[j] == maxCount:                    #if-Schleife: wenn aktuelle Stelle im Counter gleich der maximalen Anzahl an Häufigkeiten
            freqP.append(Text[j:j+k])               #nimm die zum Counter passende Stelle im Text im Array freqP auf
    #remove duplicates                      #lösche durch die Funktion set() die Duplikate aus der Liste freqP und speichere durch die Funktion list() die Liste freqP neu als Liste nicht als Menge aus
    print("frequentWords: ", (list(set(freqP))))                                    #gib das neue Array freqP ohne Duplikate aus


def patternCount(Text, Pattern):                    #erstelle Funktion mit Namen patternCount, mit der Anzahl des aktuellen Patterns im Text gezählt werden soll und lege übergebene Parameter Text und Pattern fest
    count=0                                         #setze den Counter auf 0
    n=len(Text)                                     #speichere die Länge des übergebenen Parameters Text unter n ab
    m=len(Pattern)                                  #speichere die Länge des übergebenen Parameters Pattern unter m ab

    for i in range(n-m+1):                          #erstelle for-Schleife, um Text durchgehen zu können und nach gleichen k-meren wie übergebenes Pattern im Text zu suchen
        if Text[i:i+m] == Pattern:                  #if-Schleife: wenn im Text gleiches k-mer wie übergebenes Pattern
            count+=1                                #erhöhe Counter um 1
    return count                                    #gib counter zurück

