# fastFrequentWords

import math

def fastFrequentWords(text,k):  # generieren aus Sequenz und Fensterlaenge k Liste mit am haeufigsten auftretenden k-mer (Pattern)
    frequentPatterns = []   # leere Liste fuer gefundene Pattern
    frequencyArray = computingFrequencies(text,k)   # Liste mit Anzahl der Haeufigkeiten der gefundenen k-mere
    if frequencyArray == ["X"]: # invalid Symbol, behandelt Ausnahmen fuer Symbole, die nicht A,C,G,T sind
        print("Stay genetic! Enter a valid symbol (A,C,G,T) !")
        return 0
    maxCount = max(frequencyArray)  # Index des am haufigsten auftretenden k-meres
    for i in range(0, (int(math.pow(4, k)))):   # Schleife ueber Laenge des FrequencyArrays 4^k
        if frequencyArray[i] == maxCount:   # waehle Index des am haufigsten auftretenden k-mers
            pattern = numberToPattern(i,k)  # generiere Pattern aus Indices und Fenstergroesse k mit numberToPattern
            frequentPatterns.append(pattern)    # fuege Pattern zu Liste mit gefundenden Pattern hinzu
    #remove dublicates nicht notwendig, weil nur die Indices der am haeufigsten auftretenden k-mere verwendet werden
    print("fastFrequentWords: ", frequentPatterns)   # Ausgabe

def computingFrequencies(text,k):   # generiere Frequency Array
    frequencyArray = [] # leere Liste fuer k-mere im Text
    for i in range(0, (int(math.pow(4, k)))):
        frequencyArray.append(0)    # fuelle leeres Frequency Array mit Nullen
    for i in range(0, (len(text)-k+1)): # iteriere ueber Laenge des Textes - Fenstergroesse, + 1 weil Index bei 0 startet
        pattern = text[i:i+k]   # betrachte Fenster ueber Text mit Laenge k
        number = patternToNumber(pattern)    # wandle Pattern in Zahl um
        if number == "XXX":     # invalid Symbol
            error = ["X"]
            return error
        frequencyArray[number] = frequencyArray[number]+1   # gehe zu naechstem Index
    return frequencyArray

def patternToNumber(pattern):   # wandle Pattern in Zahl um, fuer computingFrequencies
    if not pattern: # falls Pattern leer
        return 0
    symbol = pattern[-1]    # letztes Symbol des Patterns
    prefix = pattern[:-1]   # Prefix des Patterns
    number = symbolToNumber(symbol)     # wandle letztes Symbol des Patterns in zugehoerige Zahl um
    if number == 4:     # invalid Symbol
        error = "XXX"
        return error
    return 4* patternToNumber(prefix) + number  # generiere Zahl fuer Pattern, Basis 4 wegen 4 Nukleotiden

def symbolToNumber(symbol): # wandle Nukleotid in entsprechenden Repraesentanten um, fuer patternToNumber
    if symbol == "a":
        return 0
    elif symbol == "c":
        return 1
    elif symbol == "g":
        return 2
    elif symbol == "t":
        return 3
    else:
        return 4 #invalid symbol

def numberToPattern(index,k):   # wandle Zahl in Pattern um, fuer fastFrequentWords
    if k == 1:  # falls Fenstergroesse k=1, sofort Zahlen zu Basen codieren
        return numberToSymbol(index)
    prefixIndex = math.floor(index / 4)     # Quotient aus Index des Frequency Arrays und Basis 4, Prefix des Patterns
    remainder = index % 4   # Rest aus Index modulo 4(Basis), aequivalent zu patternToNumber(Prefix(Pattern))
    symbol = numberToSymbol(remainder)  # wandle Zahl in Repraesentant des Nukleotids um
    prefixPattern = numberToPattern(prefixIndex, k-1)   # rekursives Vorgehen fuer Prefix des Patterns mit verringerter Fenstergroesse
    return prefixPattern + symbol   # uebergebe konkatenierte Nukleotide = Pattern

def numberToSymbol(number): # wandle Repraesentanten in zugehoeriges Nukleotid um, fuer numberToPattern
    symb = ""
    if   number == 0:
        symb = "a"
    elif number == 1:
        symb = "c"
    elif number == 2:
        symb = "g"
    else:   # number > 3 werden schon in symbolToNumber abgefangen, siehe invalid Symbol
        symb = "t"
    return symb

def main():
    fastFrequentWords("ACGAAAAGGGGCCCCTTTTTTTTACGT", 3)

if __name__ == '__main__':
    main()