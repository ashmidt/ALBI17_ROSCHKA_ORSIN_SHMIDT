## Installation
1. install pip3
2. pip install biopython
3. pip install algorithms
4. setup python interpetator

## Components
1. fastFrequentWords.py
2. findingfrequentwordsbysorting.py
3. frequentWords.py
4. run_ori_finder.py
5. Thermotoga_petrophila.fna
6. Vibrio_cholerae.fna

## Requirments
python3.6

## How to start
1. start run_ori_finder.py

## Tests
Thermotoga_petrophila
frequentWords:  ['acctaccac']
Zeit gebraucht:  0.07890975117962709
fastFrequentWords:  ['acctaccac']
Zeit gebraucht:  0.14657653883368774
findingfrequentwordsbysorting:  ['acctaccac']
Zeit gebraucht:  0.03358753126933192
Vibrio_cholerae
frequentWords:  ['atgatcaag', 'ctcttgatc', 'tcttgatca', 'cttgatcat']
Zeit gebraucht:  0.12521402172361312
fastFrequentWords:  ['atgatcaag', 'ctcttgatc', 'cttgatcat', 'tcttgatca']
Zeit gebraucht:  0.11905346181012694
findingfrequentwordsbysorting:  ['atgatcaag', 'ctcttgatc', 'tcttgatca', 'cttgatcat']
Zeit gebraucht:  0.022707063393351268

## Problems
if startet on linux, check if FASTA file is in right codding (UTF-8)
