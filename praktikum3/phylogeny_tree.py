from Bio import SeqIO                                       #SeqIO zum Einlesen der fasta-Datei
from Bio import Phylo                                       #Phylo zum Lesen der Newick-Datei und Umschreiben ins Nexus-Formats
from io import StringIO                                     #Zusäzlich Tree in Newick Format zu speichern
from math import log                                        #Jukes-kantor Distanz zu berechnen
import operator                                             #um auf ein Objekt aus deer DIstanzmatrix zuzugreifen, statt nur auf Indexen

SEQUENCE_DESCRIPTION ={}                                    #leeres Set (Menge)/Dictionary für die Sequenz-ID's und dazugehörige Metadaten
CLUSTERS={}                                                 #leeres Set für die Cluster der Sequenzen bei UPGMA in entsprechender Reihenfolge!!

def create_two_distance_matrix(filename):
    for record in SeqIO.parse(filename, "fasta"):           ##für das Einlesen mehrerer Seequenzen im fasta-Format innerhalb einer Datei benutzen wir Seq.IO.parse
        split_fasta_header =record.description.split('/')
        SEQUENCE_DESCRIPTION[record.id]=[split_fasta_header[1],split_fasta_header[2]]  #Speichere unter SEQUENCE_DESCRIPTION für jede vorkommende Sequenz-ID die Sequenz-ID (unter der jeweiligen ID?)

    hamming_distance_matrix_values = []                  #leere Liste für die Distanzmatrizenwerte
    hamming_distance_matrix_labels = []                  #leere Liste für die Distanzmatrizenlabels mit den Seq_ID
    jc_distance_matrix_values = []                          #leere Liste für die Jukes-Cantor-Distanzmatrizenwerte
    jc_distance_matrix_labels =[]                           #leere Liste für die Jukes-Cantor-Distanzmatrizenlabels

    records = list(SeqIO.parse(filename, "fasta"))          #Separiere alle Alignments durch Einteilung auf Listenplätze in records
    temp_records = list(records)                            #Speichere die Alignments in temp_records als neue Objekt
    len_seq = len(records[0].seq)                           #die Alignmentlänge (immer gleich) der ersten Sequenz in records wird unter len_seq gespeichert

    for record1 in records:                                 #für jedes Element in records
       del temp_records[0]                                  #lösche das erste Element aus dem temporären Zwischenspeicher temp_records
       for record2 in temp_records:                         #für jedes Element im temporären Zwischenspeicher temp_records (gleiche Seq wurde ein Schritt vorher gelöscht)
           distance =0                                      #setze die Distanz auf 0 (Initialisierung)
           for letter1, letter2 in zip(record1.seq, record2.seq): ##für jeden Buchstaben in Seq1 und Seq2 (zip() liest die Seq gleichzeitig ein)
               if letter1==letter2 or letter1 =="-" or letter2 =='-': #falls die Buchstaben gleich sind oder Gaps ("-") enthalten sind:
                   pass
               else:
                   distance =distance+1                     #erhöhe die Distanz um 1

           hamming_distance_matrix_values.append(float(distance)/len_seq) ##füge berechnete Distanz für aktuelle Sequenzen in Hamming-Distanz-Liste ein
           hamming_distance_matrix_labels.append([record1.id, record2.id]) ##speichere Seq_Id's der aktuellen Sequenzen in Liste der Form ["record1.id, record2.id"]

           jc_distance_matrix_values.append( -(float(3)/4)*log(1 - (4/3)*float(distance) / len_seq)) #füge berechnete Distanz für aktuelle Sequenzen in JukesKantor-Distanz-Liste ein
           jc_distance_matrix_labels.append([record1.id, record2.id])   ##speichere Seq_Id's der aktuellen Sequenzen in Liste der Form ["record1.id, record2.id"]

    return hamming_distance_matrix_values, hamming_distance_matrix_labels, jc_distance_matrix_values, jc_distance_matrix_labels


def create_and_save_upgma_tree(distance_matrix_values, distance_matrix_labels, output_name):
    save_last_id = ""
    count_distance=1                                           #evolutionäre Distanz für Figtree
    tree_str=""
    while len(distance_matrix_values) > 1:                     #Abbruchbedingung für UPGMA

        min_index, min_distance = min(enumerate(distance_matrix_values), key=operator.itemgetter(1))    #Wähle kleinstes Element und nicht kleinste Nummerierung aus Distanzmatrix
        save_id1 = distance_matrix_labels[min_index][0]        #Speichere Label 1 von betrachteten Knoten
        save_id2 = distance_matrix_labels[min_index][1]        ##Speichere Label 2 von betrachteten Knoten
        try:
            host1 = SEQUENCE_DESCRIPTION[save_id1][0]
            country1 = SEQUENCE_DESCRIPTION[save_id1][0]
        except:
            pass
        try:
            host2 = SEQUENCE_DESCRIPTION[save_id2][0]
            country2 = SEQUENCE_DESCRIPTION[save_id2][0]
        except:
            pass

        if save_id1 not in CLUSTERS and save_id2 not in CLUSTERS:
            tree_str=tree_str+"("+save_id1+"[&host='"+host1+"',country='"+country1+"']"+":1, "+ save_id2+"[&host='"+host2+"',country='"+country2+"']"+":1):"+str(count_distance)+", " # Nexus
        elif save_id1 not in CLUSTERS and save_id2 in CLUSTERS:
            count_distance = count_distance+1                   #Konnte Knoten noch nicht geclustert werden + 1 evolutionäre Distanz
            tree_str="("+tree_str+save_id1+"[&host='"+host1+"',country='"+country1+"']"+":"+str(count_distance)+"):1, " #Nexus
        elif save_id1 in CLUSTERS and save_id2 not in CLUSTERS:
            count_distance = count_distance + 1
            tree_str = "(" + tree_str + save_id2 + "[&host='"+host2+"',country='"+country2+"']"+":" + str(count_distance) + "):1, "
        elif save_id1 in CLUSTERS and save_id2 in CLUSTERS:
            tree_str="("+(tree_str.rstrip(', '))+"):1, "
            count_distance = count_distance + 1
        print(tree_str)
        distance_matrix_labels.pop(min_index)                   #Lösche Label aus Liste, damit es in nächster Iteration nicht vorkommt
        distance_matrix_values.pop(min_index)                   #Lösche Wert aus Liste, damit es in nächster Iteration nicht vorkommt
        restart =True
        while restart:
            restart=False
            for index, ids in enumerate(distance_matrix_labels):    # Berechne neue Distanzen
                if save_id1 in ids or save_id2 in ids:
                                                                    # Betrachte 1. oder 2. Stelle, ob zuletzt geclusterter Knoten in Liste ist, intressieren uns für nicht geclusterte Labels und ihre Positionen in der Liste
                    if save_id1 == ids[0] :                         # Wollen Werte speichern, die wir neu berechnen müssen
                        save_other_id = ids[1]                      #markiere Label, welches nicht zuletzt geclustert wurde mit verschiedenen Fällen
                        first_flag =True                            #wenn nicht zuletzt geclustertes label an Position 1 steht, markiere Positon 1
                    if save_id1 == ids[1]:
                        save_other_id = ids[0]
                        first_flag = True
                    if save_id2 == ids[0]:
                        save_other_id = ids[1]
                        first_flag = False
                    if save_id2 == ids[1]:
                        save_other_id = ids[0]
                        first_flag = False

                    save_first_id_value = distance_matrix_values[index] # Setzte erstes Element aus paarweisen Distanzen fest
                    distance_matrix_values.pop(index)               #Lösche Wert aus Liste, damit es in nächster Iteration nicht vorkommt
                    distance_matrix_labels.pop(index)               #Lösche Label aus Liste, damit es in nächster Iteration nicht vorkommt
                    for index2, ids2 in enumerate(distance_matrix_labels):  #Suche Distanzpaar, welches festegesetztes Element enthält
                        if (save_id1 in ids2 or save_id2 in  ids2) and (save_other_id in ids2): # gleicher Filter wie oben, betrachten wieder nur die Knoten, welche Beziehung zum zuletzt geclusterten Cluster haben
                            save_second_id_value = distance_matrix_values[index2]   # Setzte zweites Element aus paarweisen Distanzen fest, damit es bei nächster Iteration nicht gefunden wird
                            distance_matrix_values.pop(index2)       #Lösche Wert aus Liste, damit es in nächster Iteration nicht vorkommt
                            distance_matrix_labels.pop(index2)       #Lösche Label aus Liste, damit es in nächster Iteration nicht vorkommt

                            if save_id1 not in CLUSTERS and save_id2 not in CLUSTERS: #Wenn beide nicht in Cluster

                                CLUSTERS.update({save_id1+"_"+save_id2:2})  # erstelle ien Cluster, berechne Wert, 2, weil jetzt 2 Sequenzen in dem Cluster enthalten sind
                                distance_matrix_labels.append([save_other_id,save_id1+"_"+save_id2]) #Label ändern, da gerade zu Cluster zusammengefügt
                                distance_matrix_values.append((1*save_first_id_value+1*save_second_id_value)/(1+1)) # UPGMA berechnungsvorschrift, 1 wegen 1 enthaltener Sequenz für eben erstelltes Cluster
                            if save_id1 in CLUSTERS and save_id2 not in CLUSTERS: # nur ein in Cluster

                                temp_value = CLUSTERS[save_id1] #Wert = Anzahl Sequenzen in Cluster
                                CLUSTERS.update({save_id1+"_"+save_id2: temp_value+1}) # sovoiele wie in Cluster sind + 1, weil eine sequenz nicht in Cluster
                                if first_flag: #geclusterte Lables an Stelle 2, an Stelle 1 ist Label, wowir wert neu berechnenn müssen
                                    distance_matrix_labels.append([save_other_id, save_id1 + "_" + save_id2])   # siehe oben
                                    distance_matrix_values.append((temp_value * save_first_id_value + 1 * save_second_id_value)/(temp_value+1)) # UPGMA Formel
                                else: # was wir berechnen wollen ist an 2. stelle
                                    distance_matrix_labels.append([save_other_id, save_id1 + "_" + save_id2]) # siehe oben
                                    distance_matrix_values.append((1* save_first_id_value + temp_value * save_second_id_value) / (temp_value + 1)) #Anzahl der Sequenzen (Multiplikatoren) im Zähler der UPGMA Formel vertauscht

                            if save_id1 not in CLUSTERS and save_id2 in CLUSTERS:   # wieder nur 1 geclustert
                                temp_value = CLUSTERS[save_id2] #Wert = Anzahl Sequenzen in Cluster
                                CLUSTERS.update({save_id1+"_"+save_id2: temp_value+1})  # sovoiele wie in Cluster sind + 1, weil eine sequenz nicht in Cluster
                                if first_flag: #geclusterte Lables an Stelle 2, an Stelle 1 ist Label, wowir wert neu berechnenn müssen
                                    distance_matrix_labels.append([save_other_id, save_id1 + "_" + save_id2])   #siehe oben
                                    distance_matrix_values.append((1 * save_first_id_value + temp_value * save_second_id_value) / (1+temp_value)) #siehe oben
                                else:   # was wir berechnen wollen ist an 2. stelle
                                    distance_matrix_labels.append([save_other_id, save_id1 + "_" + save_id2]) #siehe oben
                                    distance_matrix_values.append((temp_value * save_first_id_value + 1 * save_second_id_value) / (1 + temp_value)) #Anzahl der Sequenzen (Multiplikatoren) im Zähler der UPGMA Formel vertauscht

                            if save_id1 in CLUSTERS and save_id2 in CLUSTERS: #beide in Cluster

                                temp_value = CLUSTERS[save_id1] #Anzahl Sequenzen in Cluster 1
                                temp_value2 = CLUSTERS[save_id2] # Anzahl Sequenzen in Cluster 2

                                CLUSTERS.update({save_id1 + "_" + save_id2: temp_value+temp_value2}) #Berechne Wert, beide geclustert, also beide enthalten geclusterte Sequenzen
                                if first_flag:  # was wir berechnen wollen ist an 1. stelle
                                    distance_matrix_labels.append([save_other_id, save_id1 + "_" + save_id2])   #siehe oben
                                    distance_matrix_values.append((temp_value * save_first_id_value + temp_value2 * save_second_id_value) / (temp_value + temp_value2)) #  UPGMA Formel
                                else:  # was wir berechnen wollen ist an 2. stelle
                                    distance_matrix_labels.append([save_other_id, save_id1 + "_" + save_id2])   #siehe oben
                                    distance_matrix_values.append((temp_value2 * save_first_id_value + temp_value * save_second_id_value) / (temp_value + temp_value2)) #  UPGMA Formel
                            save_last_id=save_other_id  #Speichere letzten Wert, da Iteration stoppt bei einem Wert in Matrix
                            restart =True
                            break
                        if len(distance_matrix_labels) ==index2:
                            restart=False
                            break
    if save_last_id not in CLUSTERS:
        try:
            host = SEQUENCE_DESCRIPTION[save_id1][0]
            country = SEQUENCE_DESCRIPTION[save_id1][0]

        except:
            pass
        save_last_id = save_last_id+"[&host='"+host+"',country='"+country+"']"+":"+str(count_distance+1)
        tree_str=tree_str+save_last_id
    else:

        save_last_id=""
        tree_str=tree_str+save_last_id  # Baum mit letztem Wert und Distanz, +1, weil eine Distanz höher
        tree_str=tree_str.rstrip(", ")
    print(tree_str)
    tree = Phylo.read(StringIO(tree_str), "newick")         # Schreibe BaumString in Newick-Format
    Phylo.write(tree, output_name, "nexus")                 # Schreibe mit Phylo Newick in Nexus um

    return None


def main():
    filename = "50_influenza_a_nep_sequences.fa"
    hamming_values, hamming_labels, jc_values, jc_labels = create_two_distance_matrix(filename)
    print("hamming")
    create_and_save_upgma_tree(hamming_values, hamming_labels, "hammingtree.nex")
    print("Jukes_Kantor")
    create_and_save_upgma_tree(jc_values, jc_labels, "jctree.nex")


if __name__ == '__main__':
    main()
